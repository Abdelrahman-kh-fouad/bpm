// #include <Arduino.h>
#define USE_ARDUINO_INTERRUPTS false
#include <PulseSensorPlayground.h>

#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif
#include <Firebase_ESP_Client.h>
#include "addons/TokenHelper.h"
#include "addons/RTDBHelper.h"

const int wifi_led = D0;
#define WIFI_SSID "Pacman"
#define WIFI_PASSWORD "11235813"
#define API_KEY "AIzaSyB8-2WC77KDLtLWzmLzbDkP-u_oVKg20rk"
#define DATABASE_URL "https://carezone-9b9b2-default-rtdb.firebaseio.com"

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;
bool signupOK = false;
unsigned long sendDataPrevMillis = 0;

PulseSensorPlayground pulseSensor;
const int PULSE_INPUT = 0;
const int PULSE_BLINK = 13;
const int PULSE_FADE = 5;
const int THRESHOLD = 550;
byte samplesUntilReport;
const byte SAMPLES_PER_SERIAL_SAMPLE = 10;
const int OUTPUT_TYPE = SERIAL_PLOTTER;

void setup()
{
	Serial.begin(115200);
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
	Serial.print("Connecting to Wi-Fi");
	pinMode(wifi_led, OUTPUT);
	while (WiFi.status() != WL_CONNECTED)
	{
		digitalWrite(wifi_led, HIGH);
		delay(20);
		digitalWrite(wifi_led, LOW);
	}
	digitalWrite(wifi_led, HIGH);
	Serial.println();
	Serial.print("Connected with IP: ");
	Serial.println(WiFi.localIP());
	Serial.println();

	pulseSensor.analogInput(PULSE_INPUT);
	pulseSensor.blinkOnPulse(PULSE_BLINK);
	pulseSensor.fadeOnPulse(PULSE_FADE);
	pulseSensor.setThreshold(THRESHOLD);
	pulseSensor.setSerial(Serial);
	pulseSensor.setOutputType(OUTPUT_TYPE);
	samplesUntilReport = SAMPLES_PER_SERIAL_SAMPLE;

	if (!pulseSensor.begin())
	{

		for (;;)
		{
			digitalWrite(PULSE_BLINK, LOW);
			delay(50);
			digitalWrite(PULSE_BLINK, HIGH);
			delay(50);
		}
	}

	config.api_key = API_KEY;
	config.database_url = DATABASE_URL;
	if (Firebase.signUp(&config, &auth, "", ""))
	{
		Serial.println("ok");
		signupOK = true;
	}
	else
	{
		Serial.printf("%s\n", config.signer.signupError.message.c_str());
	}
	config.token_status_callback = tokenStatusCallback; // see addons/TokenHelper.h
	Firebase.begin(&config, &auth);
	Firebase.reconnectWiFi(true);
}

void loop()
{

	if (pulseSensor.sawNewSample())
	{

		/*
		   Every so often, send the latest Sample.
		   We don't print every sample, because our baud rate
		   won't support that much I/O.
		*/
		if (--samplesUntilReport == (byte)0)
		{
			samplesUntilReport = SAMPLES_PER_SERIAL_SAMPLE;
			pulseSensor.outputSample();

			if (pulseSensor.sawStartOfBeat())
			{
				int myBPM = pulseSensor.getBeatsPerMinute();
				pulseSensor.outputBeat();

				Serial.print("BPM: ");
				Serial.println(myBPM); // Print the BPM value
				if (Firebase.ready() && signupOK && (millis() - sendDataPrevMillis > 15000 || sendDataPrevMillis == 0))
				{
					sendDataPrevMillis = millis();

					if (Firebase.RTDB.setInt(&fbdo, "test/heart_beat", myBPM))
					{
						Serial.println("PASSED");
						Serial.println("PATH: " + fbdo.dataPath());
						Serial.println("TYPE: " + fbdo.dataType());
					}
					else
					{
						Serial.println("FAILED");
						Serial.println("REASON: " + fbdo.errorReason());
					}
				}
			}
		}
	}

	delay(10);
}
